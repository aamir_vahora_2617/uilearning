package com.example.uilearning.signUp;

import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.uilearning.R;
import com.example.uilearning.util.ImageUrl;
import com.squareup.picasso.Picasso;

public class SignUpActivity extends AppCompatActivity {

    private ImageView imgBackground;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        imgBackground = findViewById(R.id.imgBackground);
        TextView txtTerms = findViewById(R.id.txtTerms);

        Picasso.with(imgBackground.getContext()).load(ImageUrl.signupUrl).fit().into(imgBackground);

        String terms = "Terms of Use and Privacy Policy";

        SpannableString spannableString = new SpannableString(terms);
        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.GRAY);
        spannableString.setSpan(foregroundColorSpan,13,16, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        txtTerms.setText(spannableString);

    }
}